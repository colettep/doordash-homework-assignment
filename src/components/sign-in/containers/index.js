import React from 'react';

import LoginForm from '../features';

const LoginContainer = () => {
  return (
    <LoginForm />
  );
};

export default LoginContainer;
