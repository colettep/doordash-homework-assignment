import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Login extends Component {
  state = {
    inputValue: '',
  };

  handleInputChange = (event) => {
    this.setState({ inputValue: event.target.value });
  }

  render() {
    return (
      <div className="layout-body login">
        <div className="login-form">
          <form onSubmit={(event) => { event.preventDefault(); }}>
            <input
              autoFocus
              type="text"
              aria-label="type your username"
              placeholder="type your username"
              onChange={this.handleInputChange}
            />
          </form>

          <Link
            className="btn -link -fire-red -full offset -small"
            to={{
              pathname: '/chat',
              state: { username: this.state.inputValue },
            }}
          >
            Join the DoorDash Chat!
          </Link>
        </div>
      </div>
    );
  }
}
