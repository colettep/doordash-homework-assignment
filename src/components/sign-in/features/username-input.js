import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class SearchBar extends Component {
  static propTypes = {
    onInputChange: PropTypes.func,
  };

  static defaultProps = {
    onInputChange() {},
  };

  handleInputChange = (event) => {
    this.props.onInputChange(event.target.value);
  }

  render() {
    return (
      <form onSubmit={(event) => { event.preventDefault(); }}>
        <input
          autoFocus
          type="text"
          aria-label="type your username"
          placeholder="type your username"
          onChange={this.handleInputChange}
        />
      </form>
    );
  }
}
