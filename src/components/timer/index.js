import React, { Component } from 'react';

export default class ChatTimer extends Component {
  state = {
    timer: null,
    counter: 0,
  };

  componentDidMount() {
    const timer = setInterval(this.tick, 1000);
    this.setState({ timer });
  }

  componentWillUnmount() {
    clearInterval(this.state.timer);
  }

  // Todo: Get mins
  tick = () => {
    this.setState(prevState => ({ counter: prevState.counter + 1 }));
  }

  render() {
    return (
      <div>
        { `Online for ${this.state.counter} secs` }
      </div>
    );
  }
}
