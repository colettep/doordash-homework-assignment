import React from 'react';


const LoadingError = () => {
  return (
    <div>
      <p>
        We can‘t seem to find the page you‘re looking for.
      </p>
    </div>
  );
};

export default LoadingError;
