import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Sidebar extends Component {
  static propTypes = {
    listItems: PropTypes.instanceOf(Array),
    children: PropTypes.node,
    onListItemClick: PropTypes.func,
  };

  static defaultProps = {
    listItems: null,
    children: null,
    onListItemClick() {},
  };

  getListItems = (data) => {
    const sidebarList = data.map((item) => {
      return (
        <li key={item.id}>
          <button
            type="button"
            onClick={() => { this.props.onListItemClick(item.id); }}
          >
            { item.name }
          </button>
        </li>
      );
    });

    return sidebarList;
  }

  render() {
    return (
      <div className="sidebar">
        <header>
          {this.props.children}
        </header>

        <div>
          { this.props.listItems
            && <ul>{ this.getListItems(this.props.listItems) }</ul>
          }
        </div>
      </div>
    );
  }
}
