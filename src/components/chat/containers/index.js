import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

import Chat from '../features';
import ChatSidebar from '../../shared/sidebar';
import ChatTimer from '../../timer';

const chatRoomsRequested = 'http://localhost:8080/api/rooms';

export default class ChatContainer extends Component {
  static propTypes = {
    location: PropTypes.instanceOf(Object),
  };

  static defaultProps = {
    location: null,
  };

  state = {
    chatRooms: null,
    incomingMessages: null,
    outgoingMessages: null,
    roomId: null,
  };

  componentDidMount() {
    axios.get(chatRoomsRequested)
      .then((results) => {
        const chatRooms = results.data;
        this.setState({ chatRooms });
      });
  }

  handleRoomClick = (roomId) => {
    this.setState({ roomId });

    axios.get(`http://localhost:8080/api/rooms/${roomId}/messages`)
      .then((results) => {
        const incomingMessages = results.data.filter((users) => {
          return users.name !== this.props.location.state.username;
        });

        const outgoingMessages = results.data.filter((users) => {
          return users.name === this.props.location.state.username;
        });

        this.setState({ incomingMessages, outgoingMessages });
      });
  }

  handleSendMessage = (message) => {
    axios.post(`http://localhost:8080/api/rooms/${this.state.roomId}/messages`, {
      name: this.props.location.state.username,
      message,
    })
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    const { location } = this.props;

    return (
      <div className="chat-layout">
        <ChatSidebar
          listItems={this.state.chatRooms}
          onListItemClick={this.handleRoomClick}
        >
          <p>{ location.state.username }</p>
          <ChatTimer />
        </ChatSidebar>

        <Chat
          inBoundMessages={this.state.incomingMessages}
          outgoingMessages={this.state.outgoingMessages}
          onSubmit={this.handleSendMessage}
        />
      </div>
    );
  }
}
