import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class ChatThread extends Component {
  static propTypes = {
    inBoundMessages: PropTypes.instanceOf(Array),
    outgoingMessages: PropTypes.instanceOf(Array),
    onSubmit: PropTypes.func,
  };

  static defaultProps = {
    inBoundMessages: null,
    outgoingMessages: null,
    onSubmit() {},
  };

  state = {
    message: null,
  };

  getInBoundMessages = (data) => {
    const messages = data.map((text) => {
      return (
        <li key={text.id}>
          <div className="chat-bubble -incoming">{ text.message }</div>
          <p className="offset -small">{ text.name }</p>
        </li>
      );
    });

    return messages;
  }

  getOutgoingMessages = (data) => {
    const messages = data.map((text) => {
      return (
        <li key={text.id}>
          <div className="chat-bubble -outgoing">{ text.message }</div>
          <p className="offset -small">{ text.name }</p>
        </li>
      );
    });

    return messages;
  }

  handleInputChange = (event) => {
    this.setState({ message: event.target.value });
  };

  handleSubmit = (event) => {
    if (event) {
      event.preventDefault();
    }

    this.props.onSubmit(this.state.message);
  }

  render() {
    return (
      <div className="threads">
        { this.props.inBoundMessages
          && (
            <div className="threads-message">
              <ul>
                { this.getInBoundMessages(this.props.inBoundMessages) }
              </ul>
            </div>
          )
        }

        { this.props.outgoingMessages
          && (
            <div className="threads-message outgoing">
              <ul>
                { this.getOutgoingMessages(this.props.outgoingMessages) }
              </ul>
            </div>
          )
        }

        <div className="thread-from">
          <form onSubmit={this.handleSubmit}>
            <textarea
              placeholder="Type a message..."
              onChange={this.handleInputChange}
            />
          </form>

          <button
            type="button"
            className="btn"
            onClick={this.handleSubmit}
          >
            Send
          </button>
        </div>
      </div>
    );
  }
}
