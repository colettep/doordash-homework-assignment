import React from 'react';
import {
  BrowserRouter,
  Route,
  Switch,
} from 'react-router-dom';

import './patterns/App.css';

import Login from './components/sign-in/containers';
import Chat from './components/chat/containers';
import LoadingError from './components/shared/loading-error';

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" component={Login} exact />
        <Route path="/chat" component={Chat} />

        <Route component={LoadingError} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
