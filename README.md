This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

*[wip] This was a fun project! Though I didn't have much time over the weekend,
my main focus was making sure functionality was in place - a user can view and
post messages in the chat room!

## Setup

**Node**
You'll need to install node JS, v8.12.0.

**NPM**
Once node is installed, you should be able to install dependencies.

```
cd ~/doordash-homework-assignment
npm install
```

## Running Locally

To run the site locally, use the following command:

```
# Runs the app in the development mode.
# Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
npm start
```

## Running with a Specific Server

You will need to clone DoorDash Front-end-interview repo and follow their
`getting started` directions to see chatroom data.

```
cd ~/Frontend-Interview-Project-Master
npm install
```

To run the server locally, use the following command:

```
# Open [http://localhost:8080](http://localhost:8080)
npm start
```
